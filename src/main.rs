use rand::seq::SliceRandom;
use rand::thread_rng;

use indicatif::ProgressBar;

use term;

use std::fmt;


#[derive(Copy, Clone, Debug, Default, PartialEq, Eq)]
enum Color {
    #[default]
    White = 0,
    Yellow = 1,
    Green = 2,
    Blue = 3,
    Red = 4,
    Rainbow = 5,
}

const COLORS: [Color; 6] = [Color::White, Color::Yellow, Color::Green, Color::Blue, Color::Red, Color::Rainbow];

#[derive(Copy, Clone, Default)]
struct Card {
    num: u8,
    color: Color,
}

impl Card {
    fn matches_clue(&self, clue: ClueKind) -> bool {
        match clue {
            ClueKind::ColorClue(c) => self.color == c || self.color == Color::Rainbow,
            ClueKind::NumberClue(n) => self.num == n,
        }
    }

    const fn to_bitmask(&self) -> u32 {
        assert!(self.num >= 1 && self.num <= 5);
        1 << ((self.num as usize - 1) * 6 + self.color as usize)
    }

    fn pretty_print(&self) {
        let mut t = term::stdout().unwrap();
        if let Some(color) = match self.color {
            Color::White => Some(term::color::WHITE),
            Color::Yellow => Some(term::color::YELLOW),
            Color::Green => Some(term::color::GREEN),
            Color::Blue => Some(term::color::BLUE),
            Color::Red => Some(term::color::RED),
            Color::Rainbow => None,
        } {
            t.bg(color).unwrap();
            t.fg(term::color::BLACK).unwrap();
            print!(" {} ", self.num);
        } else {
            t.bg(term::color::YELLOW).unwrap();
            t.fg(term::color::RED).unwrap();
            print!("▄");
            t.bg(term::color::WHITE).unwrap();
            t.fg(term::color::BLACK).unwrap();
            print!("{}", self.num);
            t.bg(term::color::GREEN).unwrap();
            t.fg(term::color::BLUE).unwrap();
            print!("▄");
        }
        t.reset().unwrap();
    }
}

impl fmt::Debug for Card {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.num, match self.color {
            Color::White => "white",
            Color::Yellow => "yellow",
            Color::Green => "green",
            Color::Blue => "blue",
            Color::Red => "red",
            Color::Rainbow => "rainbow",
        })
    }
}

#[derive(Copy, Clone, Debug, Default)]
struct CardSet {
    cards: u32,
    doubles: u32,
    triples: u32,
}

impl CardSet {
    const fn from_card(c: Card) -> Self {
        Self {
            cards: c.to_bitmask(),
            doubles: 0,
            triples: 0,
        }
    }

    const fn full_deck() -> Self {
        Self {
            cards: (1 << 30) - 1,
            doubles: (1 << 24) - 1,
            triples: (1 << 6) - 1,
        }
    }

    const fn from_clue_kind(clue_kind: ClueKind) -> Self {
        let bitmask = match clue_kind {
            ClueKind::ColorClue(color) => 
                (((1 << 24) + (1 << 18) + (1 << 12) + (1 << 6) + 1) << color as usize) |
                (((1 << 24) + (1 << 18) + (1 << 12) + (1 << 6) + 1) << 5),
            ClueKind::NumberClue(num) => {
                assert!(num > 0 && num <= 5);
                ((1 << 6) - 1) << (((num as usize) - 1) * 6)
            },
        };
        Self {
            cards: bitmask,
            doubles: bitmask,
            triples: bitmask,
        }
    }

    fn from_table(table: &Table) -> Self {
        let mut bitmask = 0;
        for color in COLORS {
            for num in 1..=table[color as usize] {
                bitmask |= Card { num, color }.to_bitmask();
            }
        }
        Self {
            cards: bitmask,
            doubles: bitmask,
            triples: bitmask,
        }
    }

    fn playable_cards(table: &Table) -> Self {
        let mut bitmask = 0;
        for color in COLORS {
            if table[color as usize] < 5 {
                bitmask |= Card { num: table[color as usize] + 1, color }.to_bitmask();
            }
        }
        Self {
            cards: bitmask,
            doubles: bitmask,
            triples: bitmask,
        }
    }

    const fn contained_in(&self, other: CardSet) -> bool {
        assert!(self.cards != 0);
        self.cards & other.cards == self.cards && 
            self.doubles & other.doubles == self.doubles && 
            self.triples & other.triples == self.triples
    }

    fn remove(&mut self, other: CardSet) {
        *self = self.difference(other);
    }

    fn difference(&self, mut other: CardSet) -> Self {
        other.intersect(*self);
        Self {
            cards: (self.cards ^ other.cards) | (self.doubles ^ other.doubles) | (self.triples ^ other.triples),
            doubles: (self.doubles ^ other.cards) | (self.triples ^ other.doubles),
            triples: self.triples ^ other.cards,
        }
    }

    fn intersect(&mut self, other: CardSet) {
        //println!("{:030b}\n{:030b}", self.cards, other.cards);
        self.cards &= other.cards;
        self.doubles &= other.doubles;
        self.triples &= other.triples;
    }

    fn union(&self, other: CardSet) -> Self {
        Self {
            cards: self.cards | other.cards,
            doubles: self.doubles | (self.cards & other.cards) | other.doubles,
            triples: self.triples | (self.doubles & other.cards) | (self.cards & other.doubles) | other.triples,
        }
    }

    fn from_vec(vec: &Vec<Card>) -> Self {
        vec.iter().fold(CardSet { cards: 0, doubles: 0, triples: 0 }, |acc, c| acc.union(CardSet::from_card(*c)))
    }

    fn endangered_cards(&self) -> Self {
        let bitmask = self.cards ^ self.doubles;
        Self {
            cards: bitmask,
            doubles: bitmask,
            triples: bitmask,
        }
    }

    fn to_vec(&self) -> Vec<Card> {
        let mut deck = vec![];

        for num in 1..=5 {
            for color in COLORS {
                let card = Card { num, color };
                if self.cards & card.to_bitmask() != 0 {
                    deck.push(card);
                }
                if self.doubles & card.to_bitmask() != 0 {
                    deck.push(card);
                }
                if self.triples & card.to_bitmask() != 0 {
                    deck.push(card);
                }
            }
        }

        deck
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum ClueKind {
    ColorClue(Color),
    NumberClue(u8),
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct Clue {
    target_pos: usize,
    kind: ClueKind,
    cards: Vec<bool>,
}

#[derive(Clone, Debug)]
enum Action {
    Place(usize),
    Discard(usize),
    GiveClue(Clue),
}

type Table = [u8; 6];

type Hands = [Vec<Card>; 5];

trait Bot {
    fn new(pos: usize) -> Self;

    fn init(&mut self, state: State);

    fn update(&mut self, player_pos: usize, action: Action, state: State);

    fn play(&mut self, state: State) -> Action;
}

struct Han4bit {
    pos: usize,
    info: Vec<Vec<CardSet>>,
    clued: Vec<Vec<u8>>,
    last_xor: u8,
    next_move: Option<Action>,
    waiting_clue_completion: bool,
    last_is_discardable: bool,
    last_table: Table,
    unseen_cards: CardSet,
}

impl Han4bit {
    fn clue_to_bits(player_pos: usize, clue: Clue) -> u8 {
        assert!(player_pos != clue.target_pos);
        let res = (((clue.target_pos + 5 - player_pos - 1) as u8 % 5) << 2) +
            (match clue.kind { ClueKind::ColorClue(_) => 0, _ => 1 } << 1) +
            if *clue.cards.last().unwrap() { 1 } else { 0 };
        assert!(res < 16);
        res
    }

    fn bits_to_info(bits: u8, table: &Table) -> CardSet {
        assert!(bits < 16);
        let mut bitmask = 0;
        match bits {
            0b0000 => {
                bitmask = CardSet::from_table(table).cards;
            },
            0b1101 => {
                for color in COLORS {
                    if table[color as usize] <= 2 {
                        bitmask |= Card { num: table[color as usize] + 3, color }.to_bitmask();
                    }
                }
            },
            0b1110 => {
                for color in COLORS {
                    if table[color as usize] <= 1 {
                        bitmask |= Card { num: table[color as usize] + 4, color }.to_bitmask();
                    }
                }
            },
            0b1111 => {
                for color in COLORS {
                    if table[color as usize] == 0 {
                        bitmask |= Card { num: 5, color }.to_bitmask();
                    }
                }
            }
            bits => {
                let color = COLORS[((bits - 0b0001) % 6) as usize];
                bitmask = Card { num: table[color as usize] + 1 + (bits - 0b0001) / 6, color }.to_bitmask();
            },
        }
        //println!("bits -> bitmask: {} -> {:b}", bits, bitmask);
        assert!(bitmask != 0);
        CardSet { cards: bitmask, doubles: bitmask, triples: bitmask }
    }

    fn info_to_bits(card: Card, table: &Table) -> u8 {
        let d = card.num.max(table[card.color as usize]) - table[card.color as usize];
        match d {
            0 => 0b0000,
            3 => 0b1101,
            4 => 0b1110,
            5 => 0b1111,
            d => 0b0001 + 6 * (d - 1) + card.color as u8,
        }
    }

    fn bits_to_command(bits: u8) -> (Option<Action>, bool) {
        assert!(bits < 16);
        match bits {
            0b1111 => (None, false),
            bits => (
                if bits & 0b0010 == 0 {
                    Some(Action::Discard((bits >> 2) as usize))
                } else {
                    Some(Action::Place((bits >> 2) as usize))
                },
                bits & 0b0001 != 0
                ),
        }
    }

    fn command_to_bits(action: Action, last_is_discardable: bool) -> u8 {
        let res = match action {
            Action::GiveClue(_) => 0b1111,
            Action::Discard(idx) => {
                assert!(idx < 4);
                ((idx as u8) << 2) + if last_is_discardable { 1 } else { 0 }
            },
            Action::Place(idx) => {
                assert!(idx < 4);
                ((idx as u8) << 2) + 0b0010 + if last_is_discardable { 1 } else { 0 }
            },
        };
        assert!(res < 16);
        res
    }

    fn least_clued(&self, pos: usize) -> usize {
        self.clued[pos].iter().enumerate().min_by_key(|(_, c)| *c).unwrap().0
    }

    fn choose_clue(&self, state: &State) -> (Clue, i32) {
        let pos = (self.pos + 1) % 5;
        let (mut beauty, best_action) = state.hands[pos].iter().enumerate().map(|(idx, c)| (
                if CardSet::from_card(*c).contained_in(CardSet::playable_cards(&state.table)) {
                    100 - 5 * c.num as i32 + if c.num == 5 { 7 } else { 0 }
                } else {
                    -100
                },
                Action::Place(idx)
            )
        ).chain(
                state.hands[pos].iter().enumerate().map(|(idx, c)| (
                    if CardSet::from_card(*c).contained_in(CardSet::from_table(&state.table)) {
                        50
                    } else if CardSet::from_card(*c).contained_in(CardSet::full_deck().difference(CardSet::from_vec(&state.discard_pile)).endangered_cards()) {
                        -100 + 10 * c.num as i32
                    } else {
                        -50 + 10 * c.num as i32
                    },
                    Action::Discard(idx)
                )
            )
        ).max_by_key(|(beauty, _)| *beauty).unwrap();
        //dbg!(&best_action, beauty);
        let mut xor = if beauty < 0 && state.clues_left >= 2 {
            beauty = 0;
            0b1111
        } else {
            Han4bit::command_to_bits(best_action, CardSet::from_card(*state.hands[pos].last().unwrap()).contained_in(CardSet::from_table(&state.table)))
        };

        //let mut clued_cards = CardSet::default();
        for i in 1..=3 {
            let i = (pos + i) % 5;
            let card = state.hands[i][self.least_clued(i)];
            let bits = Han4bit::info_to_bits(card, &state.table);
            //if bits != 0b0001 || !CardSet::from_card(card).contained_in(clued_cards) {
                xor ^= bits;
            //}
            //clued_cards.add(CardSet::from_card(card));
        }

        let target_pos = (pos + (xor >> 2) as usize) % 5;
        let last_card = state.hands[target_pos].last().unwrap();
        let kind = match xor & 0b0011 {
            0b0000 => if last_card.color == Color::Rainbow {
                ClueKind::ColorClue(Color::Rainbow)
            } else {
                ClueKind::ColorClue(state.hands[target_pos].iter().filter(|c| c.color != last_card.color).next().unwrap_or(last_card).color)
            },
            0b0001 => ClueKind::ColorClue(last_card.color),
            0b0010 => ClueKind::NumberClue(state.hands[target_pos].iter().filter(|c| c.num != last_card.num).next().unwrap_or(last_card).num),
            _ => ClueKind::NumberClue(last_card.num),
        };
        (Clue {
            target_pos,
            kind,
            cards: state.hands[target_pos].iter().map(|c| c.matches_clue(kind)).collect(),
        }, beauty)
    }
}

impl Bot for Han4bit {
    fn new(pos: usize) -> Self {
        let mut info = vec![];
        for _ in 0..5 {
            info.push(vec![CardSet::full_deck(); 4]);
        }
        let mut clued = vec![];
        for _ in 0..5 {
            clued.push(vec![0; 4]);
        }
        Self {
            pos,
            info,
            clued,
            last_xor: 0,
            next_move: None,
            waiting_clue_completion: false,
            last_is_discardable: false,
            last_table: [0; 6],
            unseen_cards: CardSet::full_deck(),
        }
    }

    fn init(&mut self, state: State) {
        for j in 0..5 {
            if j != self.pos {
                for c in state.hands[j].iter() {
                    self.unseen_cards.remove(CardSet::from_card(*c));
                }
            }

            for i in 0..5 {
                if i != j && j != self.pos {
                    for cs in self.info[i].iter_mut() {
                        for c in state.hands[j].iter() {
                            cs.remove(CardSet::from_card(*c));
                        }
                    }
                }
            }
        }
    }

    fn update(&mut self, player_pos: usize, action: Action, state: State) {
        //dbg!(&action);
        if self.waiting_clue_completion {
            self.waiting_clue_completion = false;
            let idx = self.least_clued(self.pos);
            let bits = self.last_xor ^ Han4bit::command_to_bits(action.clone(), self.last_is_discardable);
            //dbg!(bits);
            self.info[self.pos][idx].intersect(Han4bit::bits_to_info(bits, &self.last_table));
            self.clued[self.pos][idx] = if bits >= 0b1101 { 1 } else { 2 };
            //self.clued[self.pos][idx] += 1;
            //println!("deduced that card #{} is {:030b}", idx, self.info[self.pos][idx].cards);
            assert!(self.info[self.pos][idx].cards != 0);
        }

        match action {
            Action::GiveClue(clue) => {
                for (i, cs) in self.info[clue.target_pos].iter_mut().enumerate() {
                    if clue.cards[i] {
                        cs.intersect(CardSet::from_clue_kind(clue.kind));
                    } else {
                        cs.remove(CardSet::from_clue_kind(clue.kind));
                    }
                    assert!(cs.cards != 0);
                }
                if clue.cards.iter().all(|b| *b) || (clue.kind == ClueKind::ColorClue(Color::Rainbow) && *clue.cards.last().unwrap()) {
                    if (player_pos + 1) % 5 == self.pos {
                        self.last_xor = Han4bit::clue_to_bits(player_pos, clue);
                        for i in 2..=4 {
                            let i = (player_pos + i) % 5;
                            if i != self.pos {
                                let idx = self.least_clued(i);
                                self.last_xor ^= Han4bit::info_to_bits(state.hands[i][idx], &state.table);
                            }
                        }
                        self.last_xor |= 0b0001; // let's be pessimists
                        self.next_move = Han4bit::bits_to_command(self.last_xor).0;
                    } else {
                        // TODO
                    }
                } else {
                    self.last_xor = Han4bit::clue_to_bits(player_pos, clue);
                    self.last_table = state.table;
                    //dbg!(self.last_xor);
                    for i in 2..=4 {
                        let i = (player_pos + i) % 5;
                        if i != self.pos {
                            let idx = self.least_clued(i);
                            let bits = Han4bit::info_to_bits(state.hands[i][idx], &state.table);
                            self.last_xor ^= bits;
                            //dbg!(self.last_xor);
                            self.clued[i][idx] = if bits >= 0b1101 { 1 } else { 2 };
                            //self.clued[i][idx] += 1;
                        }
                    }
                    if (player_pos + 1) % 5 == self.pos {
                        let (maybe_action, last_is_discardable) = Han4bit::bits_to_command(self.last_xor);
                        self.next_move = Some(maybe_action.unwrap_or(Action::GiveClue(self.choose_clue(&state).0)));
                        if last_is_discardable {
                            self.info[self.pos].last_mut().unwrap().intersect(CardSet::from_table(&state.table));
                        } else {
                            self.info[self.pos].last_mut().unwrap().remove(CardSet::from_table(&state.table));
                        }
                    } else if player_pos != self.pos {
                        let last_card = state.hands[(player_pos + 1) % 5].last().unwrap();
                        self.last_is_discardable = CardSet::from_card(*last_card).contained_in(CardSet::from_table(&state.table));
                        self.waiting_clue_completion = true;
                    }
                }
            },
            Action::Place(idx) | Action::Discard(idx) => {
                self.info[player_pos].remove(idx);
                self.clued[player_pos].remove(idx);
                if self.info[player_pos].len() < state.hands[player_pos].len() {
                    self.info[player_pos].insert(0, self.unseen_cards); // FIXME
                    self.clued[player_pos].insert(0, 0);
                    if player_pos != self.pos {
                        self.unseen_cards.remove(CardSet::from_card(state.hands[player_pos][0]));
                        for cs in self.info[self.pos].iter_mut() {
                            cs.intersect(self.unseen_cards);
                        }
                    }
                }
            },
        }
        //dbg!(&self.clued);

        //println!("info:");
        //for c in &self.info[self.pos] {
            //println!("{:030b}", c.cards);
        //}
    }

    fn play(&mut self, state: State) -> Action {
        if let Some(action) = &mut self.next_move {
            let action = action.clone();
            self.next_move = None;
            action
        } else if let Some(idx) = self.info[self.pos].iter().enumerate()
            .filter(|(_, cs)| cs.contained_in(CardSet::playable_cards(&state.table))).map(|(idx, _)| idx).next() {
            Action::Place(idx)
        } else {
            let (best_clue, beauty) = self.choose_clue(&state);
            if state.clues_left > 0 && beauty + 5 * state.clues_left as i32 >= 60 {
                Action::GiveClue(best_clue)
            } else if let Some(idx) = self.info[self.pos].iter().enumerate()
                .filter(|(_, cs)| cs.contained_in(CardSet::from_table(&state.table))).map(|(idx, _)| idx).next() {
                Action::Discard(idx)
            } else if state.clues_left > 0 && beauty >= 0 {
                Action::GiveClue(best_clue)
            } else if let Some(idx) = self.info[self.pos].iter().enumerate() // FIXME
                .filter(|(_, cs)| !cs.contained_in(CardSet::full_deck().difference(CardSet::from_vec(&state.discard_pile)).endangered_cards())).map(|(idx, _)| idx).next() {
                Action::Discard(idx)
            } else {
                Action::Discard(self.info[self.pos].len()-1)
            }
        }
    }
}

struct Deck {
    cards: Vec<Card>,
}

impl Deck {
    fn random() -> Self {
        let mut cards = CardSet::full_deck().to_vec();

        let mut rng = thread_rng();
        cards.as_mut_slice().shuffle(&mut rng);

        Deck { cards }
    }

    fn draw(&mut self) -> Option<Card> {
        self.cards.pop()
    }

    fn draw4(&mut self) -> Vec<Card> {
        vec![self.draw().unwrap(), self.draw().unwrap(), self.draw().unwrap(), self.draw().unwrap()]
    }

    fn size(&self) -> usize {
        self.cards.len()
    }
}

#[derive(Debug)]
#[allow(dead_code)]
struct State {
    table: Table,
    hands: Hands,
    clues_left: u8,
    errors: u8,
    deck_left: usize,
    turns_left: Option<usize>,
    discard_pile: Vec<Card>,
}

struct Game<B: Bot> {
    players: [B; 5],
    deck: Deck,
    table: Table,
    hands: Hands,
    cp: usize,
    clues_left: u8,
    errors: u8,
    turns_left: Option<usize>,
    discard_pile: Vec<Card>,
}

impl<B: Bot> Game<B> {
    fn new(mut deck: Deck) -> Self {
        Game {
            players: [B::new(0), B::new(1), B::new(2), B::new(3), B::new(4)],
            table: [0; 6],
            hands: [deck.draw4(), deck.draw4(), deck.draw4(), deck.draw4(), deck.draw4()],
            deck,
            cp: 0,
            clues_left: 8,
            errors: 0,
            turns_left: None,
            discard_pile: vec![],
        }
    }

    fn random() -> Self {
        Self::new(Deck::random())
    }

    fn state(&self) -> State {
        State {
            table: self.table,
            hands: self.hands.clone(),
            clues_left: self.clues_left,
            errors: self.errors,
            deck_left: self.deck.size(),
            turns_left: self.turns_left,
            discard_pile: self.discard_pile.clone(),
        }
    }

    fn censored_state(&self, player: usize) -> State {
        let mut res = self.state();
        res.hands[player] = vec![Card::default(); self.hands[player].len()];
        res
    }

    fn play(&mut self, verbose: bool) -> (u8, u8) {
        let mut turn = 0;
        let mut t = term::stdout().unwrap();
        while self.errors < 3 && self.turns_left != Some(0) {
            let action = self.players[self.cp].play(self.censored_state(self.cp));

            if verbose {
                println!("Turn {}\n", turn);
                for i in 0..5 {
                    if i == self.cp {
                        print!(" ");
                        t.bg(term::color::BRIGHT_WHITE).unwrap();
                        t.fg(term::color::BLACK).unwrap();
                        print!(" Player {} ", i);
                        t.reset().unwrap();
                        print!(" ");
                    } else {
                        print!("   Player {}   ", i);
                    }
                }
                println!("      Clues: {}", self.clues_left);
                for i in 0..5 {
                    print!(" ");
                    for _ in 0..(4 - self.hands[i].len()) {
                        print!("___");
                    }
                    for c in &self.hands[i] {
                        c.pretty_print();
                    }
                    print!(" ");
                }
                println!("      Errors: {}\n", self.errors);

                println!("      Table               Discard Pile");
                print!(" ");
                for color in COLORS {
                    Card { color, num: self.table[color as usize] }.pretty_print();
                }
                print!("      ");
                for c in &self.discard_pile {
                    c.pretty_print();
                }
                println!();
                println!();
                print!(" => Player {} ", self.cp);
                match &action {
                    Action::Place(idx) => {
                        print!("places card #{} = ", idx);
                        self.hands[self.cp][*idx].pretty_print();
                    },
                    Action::Discard(idx) => {
                        print!("discards card #{} = ", idx);
                        self.hands[self.cp][*idx].pretty_print();
                    },
                    Action::GiveClue(clue) => {
                        print!("tells Player {} that cards ", clue.target_pos);
                        for (i, _) in clue.cards.iter().enumerate().filter(|(_, c)| **c) {
                            print!("{} ", i);
                        }
                        print!("are ");
                        match clue.kind {
                            ClueKind::NumberClue(n) => print!("{}", n),
                            ClueKind::ColorClue(c) => print!("{:?}", c),
                        }
                    }
                }
                println!();
                println!();
                println!();
            }
            match action {
                Action::GiveClue(_) => {
                    assert!(self.clues_left > 0);
                    self.clues_left -= 1;
                },
                Action::Discard(idx) => {
                    self.clues_left = (self.clues_left + 1).min(8);
                    self.discard_pile.push(self.hands[self.cp].remove(idx));
                    if let Some(c) = self.deck.draw() {
                        self.hands[self.cp].insert(0, c);
                    }
                },
                Action::Place(idx) => {
                    let card = self.hands[self.cp].remove(idx);
                    if self.table[card.color as usize] + 1 == card.num {
                        self.table[card.color as usize] += 1;
                        if card.num == 5 {
                            self.clues_left = (self.clues_left + 1).min(8);
                        }
                    } else {
                        self.errors += 1;
                    }
                    if let Some(c) = self.deck.draw() {
                        self.hands[self.cp].insert(0, c);
                    }
                },
            }

            if let Some(t) = self.turns_left {
                self.turns_left = Some(t-1);
            } else if self.deck.size() == 0 {
                self.turns_left = Some(5);
            }

            for i in 0..5 {
                self.players[i].update(self.cp, action.clone(), self.censored_state(i));
            }

            self.cp = (self.cp + 1) % 5;
            turn += 1;
        }

        (self.table.iter().sum(), self.errors)
    }
}

#[allow(dead_code)]
fn bench<B: Bot>(samples: u64) {
    let bar = ProgressBar::new(samples);
    let mut res_freq = [[0; 31]; 4];
    let mut score_sum = 0.0;
    let mut errors_sum = 0.0;
    for _ in 0..samples {
        let (score, errors) = Game::<B>::random().play(false);
        score_sum += score as f64;
        errors_sum += errors as f64;
        res_freq[errors as usize][score as usize] += 1;
        bar.inc(1);
    }

    bar.finish();

    println!("\n\n   avg score: {}    avg errors: {}\n",
        score_sum / (samples as f64),
        errors_sum / (samples as f64)
    );

    for i in 0..4 {
        println!("{:?}", res_freq[i]);
    }
}

fn main() {
    bench::<Han4bit>(100_000);
    //Game::<Han4bit>::random().play(true);
}
